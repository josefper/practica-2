#!/usr/bin/python3

import urllib.request


class Robot:
    def __init__(self, url):
        self.url = url
        self.inuse = False

    def retrieve(self):
        # Download data/documents from url
        try:
            if not self.inuse:
                print('Downloading URL...\n')
                self.content = urllib.request.urlopen(self.url)
                self.inuse = True
        except urllib.error.URLError:
            print('Could not download URL\n')

    def content(self):
        self.retrieve()
        return self.content

    def show(self):
        # Show data
        try:
            endpoint = self.content()
            data = endpoint.read().decode('utf-8')
            print(data)
        except AttributeError:
            print('Could not show data\n')
