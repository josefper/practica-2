#!/usr/bin/python3

import Robot


class Cache:
    def __init__(self):
        self.urls = {}

    def retrieve(self, url):
        # Download data/documents from url using Robot
        if url not in self.urls:
            self.urls[url] = Robot.Robot(url)

    def content(self, url):
        self.retrieve(url)
        return self.urls[url].content()

    def show(self, url):
        # Show data
        try:
            endpoint = self.content(url)
            data = endpoint.read().decode('utf-8')
            print(data)
        except AttributeError:
            print('Could not show data\n')
        except KeyError:
            print('URL not downloaded\n')

    def show_all(self):
        print('\nDownloaded URLs:')
        for url in self.urls:
            print(url)
