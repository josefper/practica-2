#!/usr/bin/python3

import Robot
import Cache


if __name__ == '__main__':
    # Show content from two websites
    print('Class Robot\n')
    robot1 = Robot.Robot('https://www.urjc.es')
    print(robot1.url)
    robot1.show()  # Not only download, also show it
    robot1.retrieve()  # This line must not work
    robot1.retrieve()  # This line must not work

    robot2 = Robot.Robot('https://www.wikipedia.es')
    robot2.show()

    # Show content from two websites with cache
    print('Class Cache\n')
    mycache = Cache.Cache()
    mycache.retrieve('https://www.urjc.es')
    mycache.show('https://www.wikipedia.es')
    mycache.show_all()
